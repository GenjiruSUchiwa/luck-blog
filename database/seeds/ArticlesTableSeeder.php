<?php
/**
 * Created by PhpStorm.
 * User: Aristide Herve
 * Date: 29/10/2019
 * Time: 10:57
 */
use App\Models\Article;

class ArticlesTableSeeder extends \Illuminate\Database\Seeder
{
    /**
     *
     */
    public function run(): void
    {
        $faker = Faker\Factory::create('fr_FR');
        $categoriesId= random_int(1, 10);

        for($i=0; $i <20; $i++) {
            $article = new Article();
            $article->name = $faker->sentences( 1,  true);
            $article->social_media_title = $faker->sentences( 1,  true);
            $article->social_media_image = $faker->image('public/images',640,480, null, false);
            $article->content = $faker->paragraphs(5,  true);
            $article->meta_description = $faker->realText(250);

            $article->status = $faker->randomElement(['DRAFT', 'PUBLISHED']);

            $article->published_date = $faker->dateTime();
            $article->featured_image = $article->social_media_image;
            $article->author()->associate(1);
            $article->save();
            $article->categories()->sync($categoriesId);




        }

    }
}