<?php
/**
 * Created by PhpStorm.
 * User: Aristide Herve
 * Date: 29/10/2019
 * Time: 13:20
 */

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    public function run():void
    {
            $faker = Faker\Factory::create('fr_FR');

            for($i=0; $i < 10; $i++)
            {
                $categories = new Category();
                $categories->name = $faker->words(1, true);
                $categories->description = $faker->sentences(6, true);
                $categories->save();
            }
    }

}