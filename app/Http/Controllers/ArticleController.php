<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests\ArticleRequest;
use App\Logic\SEO\Seo;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();

        return view('pages.admin.articles.index', compact('articles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        $tags       = Tag::orderBy('name', 'ASC')->get();
        return view('pages.admin.articles.create', compact('categories', 'tags'));
    }

    /**
     * @param ArticleRequest $request
     * @return Article
     */
    public function store(ArticleRequest $request): Article
    {

        $article = new Article ;

        $article->name = $request->name;
        $article->social_media_title = $request->social_media_title;
        $article->social_media_image = $request->featured_image;
        $article->content = $request->content;
        $article->meta_description = $request->meta_description;
        if($request->published =="on"){
        $article->status = 'PUBLISHED';
        } else {
            $article->status ='DRAFT';
        }
        //dd($request->published_date);
        $published_date = Carbon::parse($request->published_date)->format('yyyy/mm/dd hh:ii');
        $published_date_final = Carbon::createFromFormat('yyyy/mm/dd hh:ii', $published_date);

        $article->published_date = $published_date_final;
        $article->featured_image = $request->featured_image;
        $article->author()->associate(Auth::user()->id);
        dd($request);
        $article->save();
        $article->tags()->sync($request->get('tags'));
        $article->categories()->sync($request->get('categories'));
        return $article;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug): \Illuminate\Http\Response
    {
        $data = Article::findOrFail($slug);

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
     $categories = Category::all();
     $tags = Tag::all();
     $article = Article::with('tags','categories')->where('slug', $slug)->first();


     return view('pages.admin.articles.edit', compact('article', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request,  $slug)
    {
        $article = Article::where('slug', $slug)->first();
        $article->name = $request->name;
        $article->social_media_title = $request->social_media_title;
        $article->social_media_image = $request->featured_image;
        $article->content = $request->content;
        $article->meta_description = $request->meta_description;
        if($request->published =="on"){
            $article->status = 'PUBLISHED';
        } else {
            $article->status ='DRAFT';
        }
        //dd($request->published_date);
        $published_date = Carbon::parse($request->published_date)->format('yyyy/mm/dd hh:ii');
        $published_date_final = Carbon::createFromFormat('yyyy/mm/dd hh:ii', $published_date);

        $article->published_date = $published_date_final;
        $article->featured_image = $request->featured_image;
        $article->author()->associate(Auth::user()->id);
        $article->update();
        $article->tags()->sync($request->get('tags'));
        $article->categories()->sync($request->get('categories'));

        return $article;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug): ?\Illuminate\Http\Response
    {
        $data = Article::where('slug', $slug)->delete();
        redirect('pages.admin.articles.trashed');
    }

    /**
     * @return mixed
     */
    public function onTrash() {
        $articles = Article::onlyTrashed()->get();
        return view('pages.admin.articles.trashed', compact('articles'));
    }

    /**
     * Return articles inside trash
     * @return $data type = Json [description]
     */
    public function onDraft()
    {
        $data = Article::onlyTrashed()->get();

        return $data;
    }
}
